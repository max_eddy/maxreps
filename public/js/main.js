var MAXREPS = MAXREPS || {};

// Utility Functions
(function($, options) {

    var defaults = { overlay: '#overlay', message: '#message-center' },
        options = $.extend(defaults, options);

    MAXREPS.applyOverlay = function() {
        if ($('#overlay').length) {
            return;
        }

        $('body').append('<div id="overlay"></div>');

        // http://cssload.net/
        $(options.overlay).append(
            '<div id="floatingCirclesG" class="spinner">' +
               '<div class="f_circleG" id="frotateG_01"></div>' +
               '<div class="f_circleG" id="frotateG_02"></div>' +
               '<div class="f_circleG" id="frotateG_03"></div>' +
               '<div class="f_circleG" id="frotateG_04"></div>' +
               '<div class="f_circleG" id="frotateG_05"></div>' +
               '<div class="f_circleG" id="frotateG_06"></div>' +
               '<div class="f_circleG" id="frotateG_07"></div>' +
               '<div class="f_circleG" id="frotateG_08"></div>' +
          '</div>'
        );
    };


    MAXREPS.disableOverlay = function() {
        var $overlay = $(options.overlay);

        $(document).ajaxStop(function() {
            $(options.overlay).fadeOut('slow', function() {
                $(this).remove();
            });
        });
    };

    MAXREPS.displayMessage = function(type, message) {
        var messageClass;

        switch (type) {
            case 'message':
                messageClass = 'alert alert-info';
                break;

            case 'warning':
                messageClass = 'alert alert-warning';
                break;

            case 'error':
                messageClass = 'alert alert-danger';
                break;
        }

        $(options.message).append(
            '<div class="' + messageClass + '">' +
                '<button type="button" class="close" data-dismiss="alert">&times;</button>' + message +
            '</div>'
        );
    };

})(jQuery);

// Schedule Module
MAXREPS.schedule = (function($, jstz, History) {

    var WorkoutViewModel,
        DayViewModel,
        ExerciseViewModel,
        SetViewModel,
        STATUS_REST = 'rest',
        STATUS_CURRENT = 'current',
        STATUS_FINISHED = 'finished',
        STATUS_WORKOUT = 'workout',
        currentDate = new Date();

    ScheduleViewModel = function(data) {
        var self = this;

        self.beginDate = ko.observable(data.beginDate);
        self.endDate = ko.observable(data.endDate);
        self.workout = ko.observable();
        self.workoutInfoClass = ko.observable();

        self.days = ko.observableArray(data.days.map(function(day) {
            return new DayViewModel(day);
        }));

        self.dateRange = ko.computed(function() {
            return self.beginDate() + ' - ' + self.endDate();
        }, self);

        self.displayWorkout = function(data) {
            var id = data.id;

            // "rest" day
            if (!id) {
                return;
            }

            getWorkout(id).done(function(data) {
                self.workout(new WorkoutViewModel(data));
            });

            self.workoutInfoClass(getClassFromStatus(data.status));
        };
    };

    DayViewModel = function(data) {
        var self = this;

        self.name = data.name;
        self.date = ko.observable(data.date);
        self.heading = ko.computed(function() {
            return self.name + '<br /><small>' + self.date() + '</small>'
        }, self);

        // Days without workouts are considered "rest" days.
        if (!data.workouts.length) {
            data.workouts = [{ name: 'Rest', id: null }];
        }

        if (data.workouts.length === 1 && data.workouts[0].name === 'Rest') {
            self.status = STATUS_REST;
        } else {
            var date = new Date(data.date);

            // Workouts for today
            if (date.getDay() === currentDate.getDay()) {
                self.status = STATUS_CURRENT;
            }
            // Past workout days
            else if (date.getDay() < currentDate.getDay()) {
                self.status = STATUS_FINISHED;
            }
            // Future workout days
            else {
                self.status = STATUS_WORKOUT;
            }
        }

        self.infoClass = ko.observable(getClassFromStatus(self.status));

        self.workouts = ko.observableArray(data.workouts.map(function(workout) {
            workout.status = self.status;
            return workout;
        }));
    };

    WorkoutViewModel = function(data) {
        console.log(data);
        var self = this,
            i,
            length,
            date;

        self.name = data.name;
        self.exercises = ko.observableArray(data.exercises);
        self.date = data.date;

        /*for (i = 0, num = data.exercises.length; i < num; i += 1) {
            self.exercises.push(new Exercise(data.exercises[i]));
        }*/
    };

    ExerciseViewModel = function(data) {
        var self = this;

        self.name = data.name;
        self.restLength = data.rest_length;

        self.sets = ko.observableArray();
        self.startTimer = function() {

        };
    };

    SetViewModel = function(data) {
        var self = this;

        self.weight = data.weight;
        self.reps = data.reps;
        self.notes = data.notes;
        self.finshed = data.finished_at;
    };

    function getWorkout(id) {
        return $.ajax({
            url: '/workouts/' + id,
            data: 'json',
            beforeSend: MAXREPS.applyOverlay()
        }).fail(function() {
            MAXREPS.displayMessage('error', 'Unable to find workout.');
        }).always(function() {
            MAXREPS.disableOverlay();
        });
    }

    function getClassFromStatus(status) {
        if (status === STATUS_REST) {
            return 'panel panel-default';
        } else if (status === STATUS_CURRENT) {
            return 'panel panel-primary';
        } else if (status === STATUS_FINISHED) {
            return 'panel panel-success';
        } else if (status === STATUS_WORKOUT) {
            return 'panel panel-info';
        }

        return '';
    }

    return {
        init: function() {
            var timezone = jstz.determine().name();

            // Get inital workout schedule
            $.ajax({
                url: '/workouts',
                data: {timezone: timezone},
                dataType: 'json',
                beforeSend: MAXREPS.applyOverlay()
            }).done(function(data) {
                var id = $.url().param('workout'),
                    schedule = new ScheduleViewModel(data);

                ko.applyBindings(schedule);

                /*if (id) {
                    $('button[data-id="' + id + '"]').trigger('click');
                }*/

            }).fail(function() {
                self.displayMessage('error', 'Sorry, something bad happened.');
            }).always(function() {
                MAXREPS.disableOverlay();
            });
        }
    };

})(jQuery, jstz, History);

$(function() {
    if (window.location.pathname === '/schedule') {
        MAXREPS.schedule.init();
    }
});
