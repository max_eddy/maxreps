<?php

class Day extends Eloquent {

    public function workouts()
    {
        return $this->belongsToMany('Workout');
    }

}
