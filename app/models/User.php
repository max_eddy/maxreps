<?php

class User extends Eloquent {
    
    protected $fillable = array('facebook_user_id', 'name');
}
