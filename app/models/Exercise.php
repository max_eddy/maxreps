<?php

class Exercise extends Eloquent {

    protected $fillable = array('name', 'num_sets', 'rest_length');

    protected $guarded = array('id', 'workout_id');

    public function workout()
    {
        return $this->belongsTo('Workout');
    }

    public function sets()
    {
        return $this->hasMany('Set');
    }

}
