<?php

class Workout extends Eloquent {

    protected $fillable = array('name', 'duration', 'days');

    protected $guarded = array('id', 'user_id');

    protected $hidden = array('created_at, updated_at');

    public function user()
    {
        return $this->belongsTo('User');
    }

    public function days()
    {
        return $this->belongsToMany('Day');
    }

    public function exercises()
    {
        return $this->hasMany('Exercise');
    }

}
