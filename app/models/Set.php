<?php

class Set extends Eloquent {

    protected $fillable = array('weight', 'reps', 'notes');

    protected $guarded = array('id', 'exercise_id');

    public function exercise()
    {
        return $this->belongsTo('Exercise');
    }

}
