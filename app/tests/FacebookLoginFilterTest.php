<?php

class FacebookLoginFilterTest extends TestCase {

    private $Facebook;

    private $UserRepository;

    private $User;

    public function setUp() {
        $this->Facebook = $this->getMockBuilder('Facebook')
            ->setMethods(array('getUser', 'api'))
            ->disableOriginalConstructor()
            ->getMock();

        $this->UserRepository = $this->getMockBuilder('UserRepository')
            ->setMethods(array('findByFacebookUserID', 'create'))
            ->getMock();

        $this->User = $this->getMock('User');
    }

    public function testUserIsRedirectedIfNotAuthtenticated()
    {
        Redirect::shouldReceive('to')->once()->with('/');

        $this->Facebook->expects($this->once())
            ->method('getUser')
            ->will($this->returnValue(0));

        $filter = new FacebookLoginFilter($this->Facebook, $this->UserRepository);

        $filter->filter();
    }

    public function testUserIsCreatedIfUserIsAuthenticated()
    {
        $this->Facebook->expects($this->once())
            ->method('getUser')
            ->will($this->returnValue(1234));

        $this->Facebook->expects($this->once())
            ->method('api')
            ->with($this->equalTo('/me'))
            ->will($this->returnValue(array('first_name' => 'Max', 'last_name' => 'Eddy')));

        $this->UserRepository->expects($this->once())
            ->method('findByFacebookUserID')
            ->with($this->equalTo(1234))
            ->will($this->returnValue(null));

        $this->UserRepository->expects($this->once())
            ->method('create')
            ->with($this->equalTo(1234), $this->equalTo('Max Eddy'))
            ->will($this->returnValue($this->User));

        $filter = new FacebookLoginFilter($this->Facebook, $this->UserRepository);
        $filter->filter();
    }
}
