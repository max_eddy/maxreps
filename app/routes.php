<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
    return View::make('homepage');
});

Route::get('/secret', array('before' => 'auth.facebook', function()
{
    echo 'THIS IS A SECRET PAGE!';
}));

Route::get('/schedule', 'ScheduleController@showSchedule');

Route::resource('workouts', 'WorkoutController');

Route::resource('workouts.exercises', 'ExerciseController');

Route::resource('workouts.exercises.sets', 'SetController');

// list workouts
// GET /workouts

// show workout
// GET /workouts/{workout-name}

// update workout
// PUT /workouts/{workout-name}

// create workout
// POST /workouts

// delete workout
// DELETE /workouts/{workout-name}

// list workout exercises
// GET /workouts/{workout-name}/exercises/

// create new exercise for workouts
// POST /workouts/{workout-name}/exercises/

// show exercise
// GET /exercises/{exercise-name}

// update exercise
// PUT /exercises/{exercise-name}

// delete exercise
// DELETE /exercises/{exercise-name}

// list exercise sets
// GET /exercises/sets

// create new exercise set
// POST /exercises/sets

// show set
// GET /sets/{id}

// update set
// PUT /sets/{id}

// delete set
// DELETE /sets/{id}


