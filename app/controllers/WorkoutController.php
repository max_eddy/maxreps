<?php

class WorkoutController extends \BaseController {

    protected $WorkoutRepo;

    protected $DayRepo;

    protected $UserRepo;

    protected $ExerciseRepo;

    protected $CurrentUser;

    protected $layout = 'master';

    public function __construct(
        WorkoutRepository $workoutRepo,
        DayRepository $dayRepo,
        ExerciseRepository $exerciseRepo,
        UserRepository $userRepo
    )
    {
        $this->beforeFilter('auth.facebook');

        $this->WorkoutRepo  = $workoutRepo;
        $this->DayRepo      = $dayRepo;
        $this->ExerciseRepo = $exerciseRepo;
        $this->UserRepo     = $userRepo;
        $this->CurrentUser  = $this->UserRepo->currentUser();
    }

    /**
     * Display a listing of the user's workouts.
     *
     * @return Response
     */
    public function index()
    {
        // Adjust date according to user's timezone.
        try {
            $timezone = Input::get('timezone');
            $date = $timezone ? new DateTime('now', new DateTimeZone($timezone)) : new DateTime();
        } catch (Exception $e) {
            $date = new DateTime();
        }

        // Calculate the dates for the beginning and end of the week.
        $currentDate = $date->format('M j, Y');

        if ($date->format('l') === 'Sunday') {
            $beginDate = $currentDate;
        } else {
            $beginDate = $date->modify('last Sunday')->format('M j, Y');
        }

        $endDate = $date->modify('this Saturday')->format('M j, Y');

        // Get the days of the week.
        $date->modify('last Sunday');
        $daysData = array();
        $days = $this->DayRepo->findAll();

        // Orgnaize workout data by day of the week.
        foreach ($days as $day) {
            $daysData[] = array(
                'id'       => $day->id,
                'name'     => $day->name,
                'date'     => $date->format('M j, Y'),
                'workouts' => $this->WorkoutRepo->findByDay($day, $this->CurrentUser)->toArray()
            );

            $date->modify('+1 day');
        }

        return Response::json(array(
            'currentDate' => $currentDate,
            'beginDate'   => $beginDate,
            'endDate'     => $endDate,
            'days'        => $daysData,
            'older'       => null,
            'newer'       => null
        ));
    }

    /**
     * Show the form for creating a new workout.
     *
     * @return Response
     */
    public function create()
    {
        $days = App::make('days');
        $data = array(
            'name' => Session::get('name') ?: null,
            'duration' => Session::get('duration') ?: 1,
            'days' => $days
        );

        foreach ($days as $day) {
            $key = $day . 'Checked';
            $data[$key] = Session::get($key) ?: null;
        }

        $this->layout->content = View::make('workouts.create', $data);
    }

    /**
     * Store a newly created workout in storage.
     *
     * @return Response
     */
    public function store()
    {
        $data = Input::all();
        $validator = Validator::make(
            $data,
            array('name' => 'required', 'duration' => 'between:1,52', 'days' => 'required|array|size|between:1,7'),
            array('between'  => 'The :attribute must be between :min - :max weeks.')
        );

        if ($validator->fails()) {
            return Redirect::action('WorkoutController@create')
                ->withErrors($validator)
                ->withInput();
        }

        $data['user_id'] = $this->UserId;

        $workout = $this->WorkoutRepo->create($data['name'], $this->UserID, $data['days'], $data['duration']);

        return Redirect::action('WorkoutController@show', array($workout->id));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        return Response::json(head($this->WorkoutRepo->findByID($id, true)->toArray()));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

}
