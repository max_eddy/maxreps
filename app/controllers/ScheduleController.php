<?php

class ScheduleController extends BaseController {

    protected $layout = 'master';

    public function __construct()
    {
        $this->beforeFilter('auth.facebook');
    }

    public function showSchedule()
    {
        $this->layout->content = View::make('schedule');
    }
}
