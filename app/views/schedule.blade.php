@section('content')
<section id="schedule" class="container">
    <div class="row">
        <div class="col-md-14">
            <h1>Schedule</h1>
        </div>
    </div>
    {{-- Errors are appended to this element --}}
    <div class="row">
        <div class="col-md-14" id="message-center">
        </div>
    </div>
    {{-- Controls for going back and forth in time --}}
    <div class="row">
        <div class="col-md-2 col-sm-2 col-xs-2">
            <ul class="pager">
                <li class="previous"><a href="#">&larr; Older</a></li>
            </ul>
        </div>
        <div class="col-md-10 col-sm-10 col-xs-10">
            <h3 class="text-center" data-bind="text: dateRange"></h3>
        </div>
        <div class="col-md-2 col-sm-2 col-xs-2">
            <ul class="pager">
                <li class="next"><a href="#">Newer &rarr;</a></li>
            </ul>
        </div>
    </div>
    {{-- A list of days of the week and the workout on those days --}}
    <div class="row" data-bind="foreach: days">
        <div class="col-md-2 day">
            <div data-bind="attr: { class: infoClass }">
                <div class="panel-heading">
                    <h4 class="panel-title text-center" data-bind="html: heading"></h4>
                </div>
                <div class="panel-body">
                    <div class="btn-group-vertical center-block" data-bind="foreach: workouts">
                        <button type="button" class="btn btn-default" data-bind="click: $root.displayWorkout, text: name, attr: { 'data-id': id }"></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
{{-- Details of the current workout --}}
<section id="workout-current" class="container">
    <div class="row">
        <div class="col-md-14">
            <div class="panel" data-bind="with: workout, attr: { class: workoutInfoClass }">
                <div class="panel-heading">
                    <h2 class="panel-title" data-bind="text: name"></h2>
                </div>
                <div class="panel-body">
                    {{-- Rest Timer --}}
                    <div class="row">
                        <div class="col-lg-1 col-md-2 col-sm-2 col-xs-4">
                            <span class="label label-info">Rest Timer</span>
                        </div>
                        <div class="col-lg-12 col-md-11 col-sm-10 col-xs-7">
                            <div class="progress">
                                <div class="progress-bar progress-bar-info" role="progressbar" style="width: 0%">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row" data-bind="foreach: exercises">
                        <div class="col-md-7">
                            <h3 data-bind="text: name"></h3>
                            <ul class="list-group" data-bind="foreach: sets">
                                <li class="list-group-item">
                                    <span data-bind="text: name"></span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@stop
