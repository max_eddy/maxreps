<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <title>Lift Tracker</title>

        <link rel="stylesheet" href="{{ asset('css/vendor/bootstrap.css') }}">
        <link rel="stylesheet" href="{{ asset('css/spinner.css') }}">
        <link rel="stylesheet" href="{{ asset('css/main.css') }}">

        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="nav-links">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">Max Reps</a>
                </div>
                <div class="collapse navbar-collapse" id="nav-links">
                    <ul class="nav navbar-nav">
                        <li><a href="#">Schedule</a></li>
                        <li><a href="#">History</a></li>
                        <li><a href="#">Stats</a></li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="#">About</a></li>
                    </ul>
                </div>
            </div>
        </nav>
        @yield('content')
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
        <script src="{{ asset('js/vendor/jstz.min.js') }}"></script>
        <script src="{{ asset('js/vendor/jquery.history.js') }}"></script>
        <script src="{{ asset('js/vendor/purl.js') }}"></script>
        <script src="{{ asset('js/vendor/bootstrap.min.js') }}"></script>
        <script src="{{ asset('js/vendor/knockout-3.0.0.js') }}"></script>
        <script src="{{ asset('js/plugins.js') }}"></script>
        <script src="{{ asset('js/main.js') }}"></script>
    </body>
</html>
