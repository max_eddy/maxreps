@section('content')
<section id="schedule" class="container">
    <div class="row">
        <div class="col-md-14">
            <h1>Workouts</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-md-14" id="message-center">
        </div>
    </div>
    <div class="row">
        <div class="col-md-2 col-sm-2 col-xs-2">
            <ul class="pager">
                <li class="previous"><a href="#">&larr; Older</a></li>
            </ul>
        </div>
        <div class="col-md-10 col-sm-10 col-xs-10">
            <h3 class="text-center" data-bind="text: dateRange"></h3>
        </div>
        <div class="col-md-2 col-sm-2 col-xs-2">
            <ul class="pager">
                <li class="next"><a href="#">Newer &rarr;</a></li>
            </ul>
        </div>
    </div>
    <div class="row" data-bind="foreach: days">
        <div class="col-md-2 day">
            <div data-bind="attr: { class: status }">
                <div class="panel-heading">
                    <h4 class="panel-title text-center" data-bind="html: heading"></h4>
                </div>
                <div class="panel-body">
                    <div class="btn-group-vertical center-block" data-bind="foreach: workouts">
                        <button type="button" class="btn btn-default" data-bind="click: $root.displayWorkout, text: name"></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-14 workout-detail">
            <div data-bind="attr: { class: status }">
                <div class="panel-heading">
                    <h2 class="panel-title" data-bind="text: name">
                </div>
                <div class="panel-body">
                </div>
            </div>
        </div>
    </div>
</section>
@stop
