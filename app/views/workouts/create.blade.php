@section('content')
<div data-role="page" id="create-workout">
    <div data-role="header">
        <h1 class="ui-title">New Workout</h1>
    </div>
    <div data-role="content">
        <ul class="errors">
        @foreach ($errors->all() as $message)
            <li>{{ $message }}</li>
        @endforeach
        </ul>
        {{ Form::open(array(
            'action' => 'WorkoutController@store',
            'method' => 'post',
            'id'     => 'new-workout'
        )) }}
            <label>Name: {{ Form::text('name', $name) }}</label>
            <label>Duration <em>weeks</em>: {{ Form::input('range', 'duration', $duration, array('min' => '1', 'max' => '52')) }}</label>
            <fieldset class="day-list" data-role="controlgroup" data-type="horizontal">
                <legend>Days:</legend>
                @foreach ($days as $day)
                    <label>{{ $day }} {{ Form::checkbox('days[' . $day . ']', '1', ${$day . 'Checked'}) }}</label>
                @endforeach
            </fieldset>
            {{ Form::submit('Create Workout') }}
        {{ Form::close() }}
    </div>
</div>
@stop
