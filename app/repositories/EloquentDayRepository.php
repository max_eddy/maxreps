<?php

class EloquentDayRepository implements DayRepository {

    public function findAll($toArray = false)
    {
        $days = Day::all();
        if ($toArray) {
            return $days->toArray();
        }

        return $days;
    }

    public function findAllByNames(array $names)
    {
        return Day::whereIn('name', $names)->get();
    }

}
