<?php

class EloquentExerciseRepository implements ExerciseRepository {

    public function create($name, $numSets, $restLength, Workout $workout)
    {
        $exercise = new Exercise();
        $exercise->name = $name;
        $exercise->num_sets = $numSets;
        $exercise->rest_length = $restLength;
        $exercise->workout()->associate($workout);
        $exercise->save();

        return $exercise;
    }

}
