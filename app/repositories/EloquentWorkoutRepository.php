<?php

class EloquentWorkoutRepository implements WorkoutRepository {

    private $Day;

    private $SetRepo;

    public function __construct(DayRepository $day, SetRepository $setRepo)
    {
        $this->DayRepo = $day;
        $this->SetRepo = $setRepo;
    }

    public function create($name, User $user, array $days, $duration = 8)
    {
        $days = $this->DayRepo->findAllByNames($days);

        $workout = new Workout();
        $workout->name = $name;
        $workout->duration = $duration;
        $workout->user()->associate($user);
        $workout->save();
        $days->each(function($day) use ($workout)
        {
            $workout->days()->attach($day);
        });

        return $workout;
    }

    public function findByID($id, $eager = false)
    {
        // Need to only get sets on a certain day, so need ot pass in date, also probably need to get
        // it from the front end.
        if ($eager) {
            return Workout::with('exercises.sets')->where('id', '=', $id)->get();
        }

        return Workout::find($id);
    }

    public function findByDay(Day $day, User $user)
    {
        return Workout::select('workouts.*')
            ->join('day_workout', 'workouts.id', '=', 'day_workout.workout_id')
            ->join('days', 'day_workout.day_id', '=', 'days.id')
            ->where('user_id', '=', $user->id)
            ->where('days.id', '=', $day->id)
            ->get();
    }

}
