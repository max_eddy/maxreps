<?php

interface WorkoutRepository {

    public function create($name, User $user, array $days, $duration = 0);

    public function findByID($id);

    public function findByDay(Day $day, User $user);

}
