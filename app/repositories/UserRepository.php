<?php

interface UserRepository {

    public function currentUser();

    public function findByName($name);

    public function findByFacebookUserID($id);

    public function create($facebookUserID, $name);

}
