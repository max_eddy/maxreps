<?php

interface SetRepository {

    public function create($weight, $reps, $notes, Exercise $exercie, DateTime $date);

}
