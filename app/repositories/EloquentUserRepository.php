<?php

class EloquentUserRepository implements UserRepository {

    protected $Facebook;

    public function __construct(Facebook $facebook)
    {
        $this->Facebook = $facebook;
    }

    public function currentUser()
    {
        if ($id = $this->Facebook->getUser()) {
            return $this->findByFacebookUserID($id);
        }

        return null;
    }

    public function findByName($name)
    {
        return User::where('name', '=', $name)->first();
    }

    public function findByFacebookUserID($id)
    {
        return User::where('facebook_user_id', '=', $id)->first();
    }

    public function create($facebookUserID, $name)
    {
        return User::create(array('facebook_user_id' => $facebookUserID, 'name' => $name));
    }
}
