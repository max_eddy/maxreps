<?php

class EloquentSetRepository implements SetRepository {

    public function create($weight, $reps, $notes, Exercise $exercise, DateTime $date)
    {
        $set = new Set();
        $set->weight = $weight;
        $set->reps = $reps;
        $set->notes = $notes;
        $set->exercise()->associate($exercise);
        $set->finished_at = $date->format('Y-m-d H:i:s');
        $set->save();

        return $set;
    }

    public static function validate(array $input)
    {

    }

    public function findByDate(Exercise $exercise, DateTime $date, $count = false)
    {
        $sets = Set::where('set_id', '=', $exercie->id);
        $sets->whereBetween(
            'finished_at',
            $beginDate->format('Y-m-d H:i:s'),
            $beginDate->modify('+1 day')->format('Y-m-d H:i:s')
        );

        if ($count) {
            $sets->count();
        }

        return $sets->get();

    }

}
