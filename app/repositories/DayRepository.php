<?php

interface DayRepository {

    public function findAll($toArray = false);

    public function findAllByNames(array $names);

}
