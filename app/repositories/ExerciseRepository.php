<?php

interface ExerciseRepository {

    public function create($name, $numSets, $restLength, Workout $workout);

}
