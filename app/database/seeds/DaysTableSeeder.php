<?php

class DaysTableSeeder extends Seeder {

    public function run()
    {
        DB::table('days')->delete();

        $days = array(
            array('name' => 'Sunday'),
            array('name' => 'Monday'),
            array('name' => 'Tuesday'),
            array('name' => 'Wednesday'),
            array('name' => 'Thursday'),
            array('name' => 'Friday'),
            array('name' => 'Saturday')
        );
        Day::insert($days);
    }

}
