<?php

class ExerciseNamesTableSeeder extends Seeder {

    public function run()
    {
        DB::table('exercise_names')->delete();

        $names = array(
            array('name' => 'Squat'),
            array('name' => 'Leg Press'),
            array('name' => 'Lunge'),
            array('name' => 'Deadlift'),
            array('name' => 'Leg Extension'),
            array('name' => 'Leg Curl'),
            array('name' => 'Standing Calf Raise'),
            array('name' => 'Seated Calf Raise'),
            array('name' => 'Bench Press'),
            array('name' => 'Chest Fly'),
            array('name' => 'Push-up'),
            array('name' => 'Pulldown'),
            array('name' => 'Pull-up'),
            array('name' => 'Bent-over Row'),
            array('name' => 'Upright Row'),
            array('name' => 'Shoulder Press'),
            array('name' => 'Shoulder Fly'),
            array('name' => 'Shoulder Shrug'),
            array('name' => 'Pushdown'),
            array('name' => 'Triceps Extension'),
            array('name' => 'Biceps Curl'),
            array('name' => 'Crunch'),
            array('name' => 'Russian Twist'),
            array('name' => 'Leg Raise'),
            array('name' => 'Back Extension')
        );

        ExerciseName::insert($names);
    }

}
