<?php

use Carbon\Carbon;

class TestWorkoutDataSeeder extends Seeder {

    public function run()
    {
        DB::table('workouts')->delete();
        DB::table('exercises')->delete();
        DB::table('sets')->delete();

        $date = new DateTime();

        $userRepo = App::make('UserRepository');
        $workoutRepo = App::make('WorkoutRepository');
        $exerciseRepo = App::make('ExerciseRepository');
        $setRepo = App::make('SetRepository');

        $user = $userRepo->findByName('Max Eddy');

        // Workouts
        $push = $workoutRepo->create('Push', $user, array('Monday'), 6);
        $pull = $workoutRepo->create('Pull', $user, array('Wednesday'), 6);
        $legs = $workoutRepo->create('Legs', $user, array('Friday'), 6);

        // Exercises
        $benchPress = $exerciseRepo->create('Bench Press', 3, 60, $push);
        $shoulderPress = $exerciseRepo->create('Shoulder Press', 3, 60, $push);
        $tricepExtension  = $exerciseRepo->create('Tricep Extension', 3, 60, $push);

        $pullUp = $exerciseRepo->create('Pull Up', 3, 60, $pull);
        $bentOverRow = $exerciseRepo->create('Bent Over Row', 3, 60, $pull);
        $shrug = $exerciseRepo->create('Shrug', 3, 60, $pull);

        $splitSquat = $exerciseRepo->create('Split Squat', 3, 60, $legs);
        $legCurl = $exerciseRepo->create('Leg Curl', 3, 60, $legs);
        $calfRaise = $exerciseRepo->create('Calf Raise', 3, 60, $legs);

        // Sets
        if ($date->format('l') === 'Sunday') {
            $date->modify('this Monday');
        } elseif ($date->format('l') !== 'Monday') {
            $date->modify('last Monday');
        }

        // Monday One week ago
        $date->modify('-1 weeks');
        $setRepo->create(160, 8, '', $benchPress, $date);
        $setRepo->create(160, 7, '', $benchPress, $date->modify('+3 mins'));
        $setRepo->create(160, 6, '', $benchPress, $date->modify('+3 mins'));

        $setRepo->create(80, 8, '', $shoulderPress, $date->modify('+5 mins'));
        $setRepo->create(80, 7, '', $shoulderPress, $date->modify('+3 mins'));
        $setRepo->create(80, 6, '', $shoulderPress, $date->modify('+3 mins'));

        $setRepo->create(50, 8, '', $tricepExtension, $date->modify('+3 mins'));
        $setRepo->create(50, 7, '', $tricepExtension, $date->modify('+3 mins'));
        $setRepo->create(50, 6, '', $tricepExtension, $date->modify('+3 mins'));

        // Wednesday one week ago
        $date->modify('next Wednesday');
        $setRepo->create(0, 8, '', $pullUp, $date);
        $setRepo->create(0, 7, '', $pullUp, $date->modify('+3 mins'));
        $setRepo->create(0, 6, '', $pullUp, $date->modify('+3 mins'));

        $setRepo->create(75, 8, '', $bentOverRow, $date->modify('+5 mins'));
        $setRepo->create(75, 7, '', $bentOverRow, $date->modify('+3 mins'));
        $setRepo->create(75, 6, '', $bentOverRow, $date->modify('+3 mins'));

        $setRepo->create(65, 8, '', $shrug, $date->modify('+3 mins'));
        $setRepo->create(65, 7, '', $shrug, $date->modify('+3 mins'));
        $setRepo->create(65, 6, '', $shrug, $date->modify('+3 mins'));

        // Friday one week ago
        $date->modify('next Friday');
        $setRepo->create(35, 8, '', $splitSquat, $date);
        $setRepo->create(35, 7, '', $splitSquat, $date->modify('+3 mins'));
        $setRepo->create(35, 6, '', $splitSquat, $date->modify('+3 mins'));

        $setRepo->create(30, 8, '', $legCurl, $date->modify('+5 mins'));
        $setRepo->create(30, 7, '', $legCurl, $date->modify('+3 mins'));
        $setRepo->create(30, 6, '', $legCurl, $date->modify('+3 mins'));

        $setRepo->create(25, 8, '', $calfRaise, $date->modify('+3 mins'));
        $setRepo->create(25, 7, '', $calfRaise, $date->modify('+3 mins'));
        $setRepo->create(25, 6, '', $calfRaise, $date->modify('+3 mins'));

        // Monday
        $date->modify('next Monday');
        $setRepo->create(160, 8, '', $benchPress, $date);
        $setRepo->create(160, 8, '', $benchPress, $date->modify('+3 mins'));
        $setRepo->create(160, 7, '', $benchPress, $date->modify('+3 mins'));

        $setRepo->create(80, 8, '', $shoulderPress, $date->modify('+5 mins'));
        $setRepo->create(80, 8, '', $shoulderPress, $date->modify('+3 mins'));
        $setRepo->create(80, 7, '', $shoulderPress, $date->modify('+3 mins'));

        $setRepo->create(50, 8, '', $tricepExtension, $date->modify('+3 mins'));
        $setRepo->create(50, 8, '', $tricepExtension, $date->modify('+3 mins'));
        $setRepo->create(50, 7, '', $tricepExtension, $date->modify('+3 mins'));

        // Wednesday
        $date->modify('next Wednesday');
        $setRepo->create(0, 8, '', $pullUp, $date);
        $setRepo->create(0, 8, '', $pullUp, $date->modify('+3 mins'));
        $setRepo->create(0, 7, '', $pullUp, $date->modify('+3 mins'));

        $setRepo->create(75, 8, '', $bentOverRow, $date->modify('+5 mins'));
        $setRepo->create(75, 8, '', $bentOverRow, $date->modify('+3 mins'));
        $setRepo->create(75, 7, '', $bentOverRow, $date->modify('+3 mins'));

        $setRepo->create(65, 8, '', $shrug, $date->modify('+3 mins'));
        $setRepo->create(65, 8, '', $shrug, $date->modify('+3 mins'));
        $setRepo->create(65, 7, '', $shrug, $date->modify('+3 mins'));

        // Friday
        $date->modify('next Friday');
        $setRepo->create(35, 8, '', $splitSquat, $date);
        $setRepo->create(35, 8, '', $splitSquat, $date->modify('+3 mins'));
        $setRepo->create(35, 7, '', $splitSquat, $date->modify('+3 mins'));

        $setRepo->create(30, 8, '', $legCurl, $date->modify('+5 mins'));
        $setRepo->create(30, 8, '', $legCurl, $date->modify('+3 mins'));
        $setRepo->create(30, 7, '', $legCurl, $date->modify('+3 mins'));

        $setRepo->create(25, 8, '', $calfRaise, $date->modify('+3 mins'));
        $setRepo->create(25, 8, '', $calfRaise, $date->modify('+3 mins'));
        $setRepo->create(25, 7, '', $calfRaise, $date->modify('+3 mins'));
    }

}
