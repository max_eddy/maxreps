<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSetsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sets', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('exercise_id')->unsigned();
            $table->float('weight');
            $table->integer('reps');
            $table->text('notes');
            $table->timestamp('finished_at');
            $table->timestamps();
            $table->foreign('exercise_id')
                ->references('id')->on('exercises')
                ->onDelete('cascade')
                ->onCascade('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sets', function(Blueprint $table)
        {
            $table->dropForeign('sets_exercise_id_foreign');
        });
        Schema::drop('sets');
    }

}
