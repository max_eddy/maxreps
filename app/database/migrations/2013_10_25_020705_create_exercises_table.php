<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExercisesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exercises', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('workout_id')->unsigned();
            $table->string('name');
            $table->integer('num_sets');
            $table->integer('rest_length');
            $table->timestamps();
            $table->foreign('workout_id')
                ->references('id')->on('workouts')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('exercises', function(Blueprint $table)
        {
            $table->dropForeign('exercises_workout_id_foreign');
        });
        Schema::drop('exercises');
    }

}
