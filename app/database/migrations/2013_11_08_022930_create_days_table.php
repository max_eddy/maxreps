<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDaysTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('days', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('name');
        });

        Schema::create('day_workout', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('workout_id')->unsigned();
            $table->integer('day_id')->unsigned();
            $table->timestamps();
            $table->foreign('workout_id')
                ->references('id')->on('workouts')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->foreign('day_id')
                ->references('id')->on('days')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('day_workout', function(Blueprint $table)
        {
            $table->dropForeign('day_workout_workout_id_foreign');
            $table->dropForeign('day_workout_day_id_foreign');
        });
        Schema::drop('days');
        Schema::drop('day_workout');
    }

}
