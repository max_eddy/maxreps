<?php

Validator::extend('weekdays', function($attribute, array $value, $parameters)
{
    $allowed = App::make('days');
    foreach ($value as $day) {
        if (!$index = array_search($day, $allowed)) {
            return false;
        }
        unset($allowed[$index]);
    }

    return true;
});
