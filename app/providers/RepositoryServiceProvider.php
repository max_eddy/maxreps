<?php

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider {

    public function register()
    {
        $this->app->bind(
            'DayRepository',
            'EloquentDayRepository'
        );

        $this->app->bind(
            'ExerciseRepository',
            'EloquentExerciseRepository'
        );

        $this->app->bind(
            'SetRepository',
            'EloquentSetRepository'
        );

        $this->app->bind(
            'UserRepository',
            'EloquentUserRepository'
        );

        $this->app->bind(
            'WorkoutRepository',
            'EloquentWorkoutRepository'
        );
    }

}
