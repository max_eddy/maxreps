<?php

use \Illuminate\Support\ServiceProvider;

class FacebookServiceProvider extends ServiceProvider {

    protected $app;

    public function register()
    {
        $this->app->bind('Facebook', function()
        {
            return new Facebook(array(
                'appId'  => Config::get('facebook.appId'),
                'secret' => Config::get('facebook.secret')
            ));
        });
    }
}
