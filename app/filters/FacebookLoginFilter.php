<?php

class FacebookLoginFilter {

    private $Facebook;

    private $UserRepo;

    public function __construct(Facebook $facebook, UserRepository $userRepo)
    {
        $this->Facebook = $facebook;
        $this->UserRepo = $userRepo;
    }

    public function filter()
    {
        $user = null;

        // Check cookie to see if user is authenticated.
        if ($facebookUserID = $this->Facebook->getUser()) {
            try {
                // Get facebook info, and in doing so verify user access token.
                $facebookUser = $this->Facebook->api('/me');

                // Store user, if they don't exist yet.
                $user = $this->UserRepo->findByFacebookUserID($facebookUserID);
                if (!$user) {
                    $user = $this->UserRepo->create(
                        $facebookUserID,
                        $facebookUser['first_name'] . ' ' . $facebookUser['last_name']
                    );
                }
            } catch (FacebookApiException $e) {
                $user = null;
            }
        }

        // User not currently authenticated, prompt login.
        if (!$user) {
            return Redirect::to('/');
        }
    }

}
